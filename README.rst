Rosetta data samples
====================

This repository contains some images from the Rosetta bit project:
https://www.nitrc.org/projects/rosetta/

You should go to the Rosetta bit project for the most up to date versions of
these files.  We keep a small subset of the files in this repository for
automated testing.

Please see the LICENSE or LICENSE.pdf files in this directory.
